<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pabhoz
 */
interface FigtherI {
    
    function getNombre(): string;

    function getHealtPoints(): float;

    function getStrenght(): float;

    function getSpeed(): float;

    function getDefense(): float;

    function setNombre($nombre);

    function setHealtPoints($healtPoints);

    function setStrenght($strenght);

    function setSpeed($speed);

    function setDefense($defense);
    
    function specialAttack(FigtherI $f);
    
    public function attack(FigtherI $f);
    
    public function getHurt(float $dmg);
}
