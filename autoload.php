<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

spl_autoload_register(function($class){

    if(file_exists("./Bridges/".$class.".php")){
        require "./Bridges/".$class.".php"; 
    }
    
    if(file_exists("./Entidades/".$class.".php")){
        require "./Entidades/".$class.".php"; 
    }
    
    if(file_exists("./Fabricas/".$class.".php")){
        require "./Fabricas/".$class.".php"; 
    }
    
    if(file_exists("./Interfaces/".$class.".php")){
        require "./Interfaces/".$class.".php"; 
    }
});
